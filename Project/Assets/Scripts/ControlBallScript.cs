using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBallScript : MonoBehaviour
{
    public BallAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            Debug.Log("ball collision");
            agent.AddReward(1);
        }
    }
}
