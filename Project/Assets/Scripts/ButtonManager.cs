using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class ButtonManager : MonoBehaviour
{
    public GameObject ball;
    public TMP_InputField forceAngle;
    public float forceMult;
    public void AddForce()
    {
        float angle = (float)Convert.ToDouble(forceAngle.text);
        Quaternion rot = Quaternion.Euler(0, -angle, 0);
        Vector3 vectorAngle =rot* new Vector3(1, 0, 0);
        Debug.Log(vectorAngle[0] + ", " + vectorAngle[1] + ", " + vectorAngle[2]);
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().AddForce(vectorAngle * forceMult);
    }
}
