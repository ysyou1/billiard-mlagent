using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class BallAgent : Agent
{
    public GameObject ControlBall;
    public GameObject CloseupCamera;

    Camera camera_main, camera_closeup;


    public float forceMult;

    public GameObject targetBalls;
    List<GameObject> targetBallList= new List<GameObject>();

    public GameObject GoalObj;

    public GameObject Q;

    public int controlOn;//0이 발사 대기, 1이 발사, 2가 발사 이후

    public int goalCnt;

    public float timeScale;

    public override void Initialize()
    {
        camera_main = Camera.main;
        camera_closeup = CloseupCamera.transform.GetComponentInChildren<Camera>();
        //base.Initialize();
        foreach(Transform tr in targetBalls.transform)
        {
            targetBallList.Add(tr.gameObject);
        }
        //ControlBall.GetComponent<Rigidbody>().AddForce(new Vector3(1000, 0, 0));
        goalCnt = 0;
        Time.timeScale = timeScale;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        //움직일 공 위치
        sensor.AddObservation(ControlBall.transform.localPosition);

        targetBallList.Clear();
        foreach (Transform tr in targetBalls.transform)
        {
            targetBallList.Add(tr.gameObject);//남은 타겟 수 갱신
        }
        //타겟 공 위치
        foreach (GameObject target in targetBallList)
        {
            sensor.AddObservation(target.transform.localPosition);
        }
        if (targetBallList.Count < 10)
        {
            for(int i = 0; i < 10 - targetBallList.Count; i++)
            {
                sensor.AddObservation(new Vector3(0, -10, 0));//공 10개에 맞춰서 공이 부족하면 0,-10,0으로 채움
            }
        }
        //Debug.Log("collectObservation");
    }
    
    public override void OnActionReceived(float[] vectorAction)
    {
        controlOn = 1;//발사 시작할 때 1

        float angle = vectorAction[0] * 0.1f;

        Quaternion rot = Quaternion.Euler(0, -angle, -10);
        Vector3 vectorAngle = rot * new Vector3(1, 0, 0);
        Q.SetActive(true);
        Vector3 QPos = rot * new Vector3(-1, 0, 0);
        Q.transform.position = QPos * 7.5f + ControlBall.transform.position;// + new Vector3(0,3,0);
        Q.transform.rotation = rot;
        CloseupCamera.transform.position = Q.transform.position + new Vector3(0, 1, 0);
        CloseupCamera.transform.rotation = Quaternion.Euler(30, -angle+90, 0);
        camera_main.enabled = false;
        camera_closeup.enabled = true;

        Debug.Log("before delay");
        StartCoroutine(BallControl_Delay(vectorAngle));
        //}

    }
    
    IEnumerator BallControl_Delay(Vector3 vectorAngle)
    {
        yield return new WaitForSeconds(1*Time.timeScale);//1초 대기


        Debug.Log("after delay");
        //Debug.Log(vectorAngle[0] + ", " + vectorAngle[1] + ", " + vectorAngle[2]);
        ControlBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ControlBall.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        ControlBall.GetComponent<Rigidbody>().AddForce(vectorAngle * forceMult, ForceMode.Impulse);

        Q.SetActive(false);
        Debug.Log(Time.timeScale);
        float time_scale = Time.timeScale;
        Time.timeScale = time_scale/10;//속도 1/10
        Debug.Log(Time.timeScale);
        yield return new WaitForSeconds(5*Time.timeScale);
        Time.timeScale = time_scale;//속도 정상화
        Debug.Log(Time.timeScale);
        camera_closeup.enabled = false;
        camera_main.enabled = true;
        AddReward(-1);
        controlOn = 2;//발사 완료 - 이제 속도 느려지면 다시 발사 대기 상태가 됨

    }
    
    public void GoalIn(bool controlball = false)
    {
        if (controlball)
        {
            Debug.Log("Goal In ControlBall");
            AddReward(-30);
            EndEpisode();
            return;
        }
        Debug.Log("Goal In");
        goalCnt++;
        AddReward(10*(goalCnt*2-1));
        //EndEpisode();
        bool flag = true;
        foreach(Transform tr in targetBalls.transform)
        {
            if (tr.gameObject.activeSelf)
            {
                flag = false;
            }
        }
        if (flag)
        {
            AddReward(200);
            EndEpisode();
        }
    }
    public override void OnEpisodeBegin()
    {
        //base.OnEpisodeBegin();
        ControlBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ControlBall.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        ControlBall.transform.localPosition = new Vector3(-7.95f, 0, 0);
        foreach(Transform tr in targetBalls.transform)
        {
            tr.GetComponent<Rigidbody>().velocity = Vector3.zero;
            tr.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            tr.localPosition = new Vector3(Random.Range(-14.5f,14.5f), 0, Random.Range(-8f,8f));
            tr.gameObject.SetActive(true);
        }
        goalCnt = 0;
        controlOn = 0;
    }
    private void Update()
    {
        if (ControlBall.GetComponent<Rigidbody>().velocity.magnitude < 0.2f&&controlOn==2)//속도가 느려지면
        {
            //멈추고
            ControlBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
            ControlBall.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            //다시 발사 준비
            controlOn = 0;
        }
        if (controlOn==0)
        {
            Debug.Log("before decision");
            RequestDecision();
            Debug.Log("requestdecision");
            controlOn = 1;
        }
    }
}
