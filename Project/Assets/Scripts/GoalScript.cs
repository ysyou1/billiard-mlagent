using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour
{
    public BallAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "ball")
        {
            agent.GoalIn();
            other.gameObject.SetActive(false);
        }else if (other.gameObject.tag == "ball_Control")
        {
            agent.GoalIn(true);
        }
    }
}
